resource "helm_release" "gitlab_runner" {
  name       = "gitlab-runner"
  repository = "https://charts.gitlab.io"
  chart      = "gitlab-runner"

  values = [yamlencode({
    image                   = var.runner_image
    gitlabUrl               = var.gitlab_url
    concurrent              = var.concurrent
    runnerRegistrationToken = var.runner_token
    runnerToken             = var.runner_token
    replicas                = var.replicas
    unregisterRunners       = var.unregister_runners

    runners = {
      name        = var.runner_name
      runUntagged = var.run_untagged_jobs
      tags        = var.runner_tags
      locked      = var.runner_locked
      }
    

    rbac = {
      create                    = var.create_service_account
      serviceAccountAnnotations = var.service_account_annotations
      serviceAccountName        = var.service_account
      clusterWideAccess         = var.service_account_clusterwide_access
    }

    nodeSelector   = var.manager_node_selectors
    tolerations    = var.manager_node_tolerations
    podLabels      = var.manager_pod_labels
    podAnnotations = var.manager_pod_annotations
  })]
}
