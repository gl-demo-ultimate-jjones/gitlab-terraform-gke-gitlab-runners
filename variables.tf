variable "gcp_project" {
  type        = string
  description = "The name of the Google Cloud Project where the cluster is to be provisioned"
}

variable "gcp_region" {
  type        = string
  default     = "us-central1"
  description = "The name of the Google region where the cluster nodes are to be provisioned"
}

variable "cluster_name" {
  type        = string
  default     = "gitlab-terraform-gke-argocd"
  description = "The name of the cluster to appear on the Google Cloud Console"
}

variable "cluster_description" {
  type        = string
  default     = "This cluster is managed by GitLab"
  description = "A description for the cluster. We recommend adding the $CI_PROJECT_URL variable to describe where the cluster is configured."
}

variable "machine_type" {
  type        = string
  default     = "n1-standard-4"
  description = "The name of the machine type to use for the cluster nodes"
}

variable "node_count" {
  default     = 3
  description = "The number of cluster nodes"
}

variable "replicas" {
  default     = 1
  description = "The number of manager pods to create"
}

variable "unregister_runners" {
  description =  "Whether to unregister runners when the cluster is destroyed"
  type        =  bool
  default     =  true
}

variable "runner_image" {
  type        = string
  description = "Docker image for the GitLab Runner"
  default     = "gitlab/gitlab-runner:latest"
}

variable "gitlab_url" {
  type        = string
  description = "URL of the GitLab instance"
  default     = "https://gitlab.com/"
}

variable "concurrent" {
  type        = number
  description = "The maximum number of concurrent jobs that the runner can execute"
  default     = 10
}

variable "runner_token" {
  type        = string
  description = "Registration token for the GitLab Runner"
  sensitive   = true
}

variable "runner_name" {
  type        = string
  description = "Name assigned to the runner"
  default     = "my-gitlab-runner"
}

variable "run_untagged_jobs" {
  type        = bool
  description = "Allow the runner to execute untagged jobs"
  default     = true
}

variable "runner_tags" {
  type        = list(string)
  description = "List of tags assigned to the runner"
  default     = ["docker", "kubernetes"]
}

variable "runner_locked" {
  type        = bool
  description = "Define if the runner is locked to specific projects"
  default     = false
}

variable "create_service_account" {
  type        = bool
  description = "Whether to create a Kubernetes service account for the runner"
  default     = true
}

variable "service_account_annotations" {
  type        = map(string)
  description = "Annotations to add to the service account"
  default     = {}
}

variable "service_account" {
  type        = string
  description = "The name of the service account for the runner"
  default     = "gitlab-runner-sa"
}

variable "service_account_clusterwide_access" {
  type        = bool
  description = "Whether the service account should have cluster-wide access"
  default     = false
}

variable "manager_node_selectors" {
  type        = map(string)
  description = "Node selectors for runner pod scheduling"
  default     = {}
}

variable "manager_node_tolerations" {
  type        = list(map(string))
  description = "Tolerations for runner pods to allow scheduling on certain nodes"
  default     = []
}

variable "manager_pod_labels" {
  type        = map(string)
  description = "Labels for the runner pods"
  default     = {}
}

variable "manager_pod_annotations" {
  type        = map(string)
  description = "Annotations for the runner pods"
  default     = {}
}
